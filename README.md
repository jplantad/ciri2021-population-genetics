CIRI 2021 - Population genetics

This project contains all the scripts for performing population genetics analyses on multiple VCF files.

1) popg_loop.R > load data and run the report scripts for each genetics

2) report_*.Rmd > compute population genomics analyses and create a pdf report for each gene using LaTeX


An example of VCF file used in the script is available : 

- 999_BST2_panTro3.vcf_fixed.vcf


Example of population genomics analyses reports for BST-2 are available :

- BST2_pan.pdf
- BST2_gor.pdf
- BST2_pon.pdf


Generate figures :

1) heatmaps.R > create heatmaps with the results extracted using popg_loop.R

2) figure_corr_TajD-length.R > create plots to vizualise the correlation between the Tajima's D values and gene length
