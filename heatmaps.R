##################
## build heatmaps 
## J Plantade
## May 2021
##################

# explore : https://jokergoo.github.io/ComplexHeatmap-reference/book/heatmap-annotations.html 

# packages
library(tidyr)
library(dplyr)
library(stringr)
library(ggplot2)

################## LEVENE TEST #################################################

# load data
all <- read.table(file = "levene_test/all_levene.txt", header = TRUE)

# drop na values
all <- all %>% 
  drop_na(Fstatistic, pvalue)

# add a log(pvalue) column
for (i in 1:length(all$gene)){
  pval <- all[i,4]
  all[i,5] <- log(pval)
}
colnames(all)[5] <- "log_pvalue"

# sort genes depending on their status : HIV-VIP, VIP, non-VIP, To Be Confirmed
class <- read.csv(file = "gene_status.txt", header = TRUE, sep = " ")
all$gene <- as.character(all$gene)
class$genes <- as.character(class$genes)
for (i in 1:length(all$gene)){
  gene_all <- all[i,1]
  for (j in 1:length(class$genes)){
    gene_class <- class[j,1]
    if (gene_all == gene_class){
      all[i,6] <- class[j,3]
    }
  }
}
colnames(all)[6] <- "id"
all <- arrange(all, all$id)

# plot the heatmap : only significant values
log_five <- log(0.05)
gg <- ggplot() +
  geom_raster(data = all, aes(x = species, y = reorder(gene, -id), fill = log_pvalue)) +
  scale_fill_viridis_c(alpha = 0.7, limits = c(min(all$log_pvalue, na.rm = TRUE), log_five), na.value = "grey") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  labs(x = "", y = "genes", tag = "B", fill = "log(pvalue)")
gg

# save the heamap
ggsave(filename = "heatmap_levene.png", plot = gg, device = png(), path = "levene_test/", )

################## TAJIMA'S D ##################################################

# load data
all <- read.table(file = "tajimaD/all_tajimaD.txt", header = TRUE)

# sort genes depending on their status : HIV-VIP, VIP, non-VIP, To Be Confirmed
class <- read.csv(file = "gene_status.txt", header = TRUE, sep = " ")
for (i in 1:length(all$gene)){
  gene_all <- all[i,1]
  for (j in 1:length(class$genes)){
    gene_class <- class[j,1]
    if (gene_all == gene_class){
      all[i,4] <- class[j,3]
    }
  }
}
colnames(all)[4] <- "id"
all <- arrange(all, all$id)

# adjust scientific notation
all$D_value_short <- formatC(all$D_value, format = "g", digits = 2)

# positive and negative values
gg <- ggplot(all, aes(x = species, y = reorder(gene, -id))) +
  geom_raster(aes(fill = D_value)) +
  scale_fill_gradient2(low = "#440154FF",
                       mid = "white",
                       high = "#FDE725FF",
                       midpoint = 0,
                       space = "Lab",
                       na.value = "grey50",
                       guide = "colourbar",
                       aesthetics = "fill") +
  geom_text(aes(label = D_value_short)) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  labs(x = "", y = "genes", fill = "D de Tajima")
gg

# save the plot
ggsave(filename = "heatmap_tajimaD.png", plot = gg, device = png(), path = "tajimaD/", )


################## WILCOXON TEST ###############################################

# load data
all <- read.table(file = "heterozygosity/all_tab_meanH.txt", header = TRUE)

# sort genes depending on their status : HIV-VIP, VIP, non-VIP, To Be Confirmed
class <- read.csv(file = "gene_status.txt", header = TRUE, sep = " ")
all$gene <- as.character(all$gene)
class$genes <- as.character(class$genes)
for (i in 1:length(all$gene)){
  gene_all <- all[i,1]
  for (j in 1:length(class$genes)){
    gene_class <- class[j,1]
    if (gene_all == gene_class){
      all[i,8] <- class[j,3]
    }
  }
}
colnames(all)[8] <- "id"
all <- arrange(all, all$id)

# add a log(pvalue) column
for (i in 1:length(all$gene)){
  pval <- all[i,7]
  all[i,9] <- log(pval)
}
colnames(all)[9] <- "log_pvalue"

# plot the heatmap : only significant values
log_five <- log(0.05)
gg <- ggplot() +
  geom_raster(data = all, aes(x = species, y = reorder(gene, -id), fill = log_pvalue)) +
  scale_fill_viridis_c(alpha = 0.7, limits = c(min(all$log_pvalue, na.rm = TRUE), log_five), na.value = "grey") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  labs(x = "", y = "genes", tag = "A", fill = "log(pvalue)")
gg

# save the plot
ggsave(filename = "heatmap_wilcoxon.png", plot = gg, device = png(), path = "heterozygosity/", )
