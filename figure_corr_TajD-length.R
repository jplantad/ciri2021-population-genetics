####################################################
## build plot with gene length and Tajima's D values 
## J Plantade
## May 2021
####################################################

# packages
library(dplyr)
library(tidyr)

# load gene length data
length <- read.table(file = "genes_length.txt", header = TRUE)
attach(length)

######################### PAN

# load Pan Taj D values
taj_pan <- read.table(file = "tajimaD/pan_tajimaD.txt", header = TRUE)
attach(taj_pan)
# extract panTro3 length
length_panTro3 <- length[,c(1,3)]
length_panTro3 <- na.omit(length_panTro3)
length_panTro3$panTro3 <- length_panTro3$panTro3/1000

# extract Pan paniscus Taj D
D <- taj_pan[species == "paniscus",1:3]
D <- na.omit(D)
# merge length and Taj D
D <- inner_join(D, length_panTro3, by = "gene")
# ggplot
gg_pani <- ggplot(data = D) +
  geom_jitter(aes(x = panTro3, y = D_value), alpha = 0.7, colour = "#440154FF") +
  geom_hline(yintercept = 0) +
  labs(x = "longueur (kb)", y = "D de Tajima", title = "Pan paniscus", tag = "A")

# extract troglodytes Taj D
D <- taj_pan[species == "t.troglodytes",1:3]
D <- na.omit(D)
# merge length and Taj D
D <- inner_join(D, length_panTro3, by = "gene")
# ggplot
gg_troglo <- ggplot(data = D) +
  geom_jitter(aes(x = panTro3, y = D_value), alpha = 0.7, colour = "#482677FF") +
  geom_hline(yintercept = 0) +
  labs(x = "longueur (kb)", y = "D de Tajima", title = "P.t.troglodytes", tag = "B")

# extract shweinfurthii Taj D
D <- taj_pan[species == "t.schweinfurthii",1:3]
D <- na.omit(D)
# merge length and Taj D
D <- inner_join(D, length_panTro3, by = "gene")
# ggplot
gg_schwein <- ggplot(data = D) +
  geom_jitter(aes(x = panTro3, y = D_value), alpha = 0.7, colour = "#404788FF") +
  geom_hline(yintercept = 0) +
  labs(x = "longueur (kb)", y = "D de Tajima", title = "P.t.schweinfurthii", tag = "C")

# extract ellioti Taj D
D <- taj_pan[species == "t.ellioti",1:3]
D <- na.omit(D)
# merge length and Taj D
D <- inner_join(D, length_panTro3, by = "gene")
# ggplot
gg_ellioti <- ggplot(data = D) +
  geom_jitter(aes(x = panTro3, y = D_value), alpha = 0.7, colour = "#33638DFF") +
  geom_hline(yintercept = 0) +
  labs(x = "longueur (kb)", y = "D de Tajima", title = "P.t.ellioti", tag = "D")

# extract verus Taj D
D <- taj_pan[species == "t.verus",1:3]
D <- na.omit(D)
# merge length and Taj D
D <- inner_join(D, length_panTro3, by = "gene")
# ggplot
gg_verus <- ggplot(data = D) +
  geom_jitter(aes(x = panTro3, y = D_value), alpha = 0.7, colour = "#287D8EFF") +
  geom_hline(yintercept = 0) +
  labs(x = "longueur (kb)", y = "D de Tajima", title = "P.t.verus", tag = "E")

######################### GOR

# load Gor Taj D values
taj_gor <- read.table(file = "tajimaD/gor_tajimaD.txt", header = TRUE)
attach(taj_gor)
# extract gorGor3 length
length_gorGor3 <- length[,c(1,5)]
length_gorGor3 <- na.omit(length_gorGor3)
length_gorGor3$gorGor3 <- length_gorGor3$gorGor3/1000

# merge length and Taj D
D <- inner_join(taj_gor, length_gorGor3, by = "gene")
# ggplot
gg_gor <- ggplot(data = D) +
  geom_jitter(aes(x = gorGor3, y = D_value), alpha = 0.7, colour = "#55C667FF") +
  geom_hline(yintercept = 0) +
  labs(x = "longueur (kb)", y = "D de Tajima", title = "G.g.gorilla", tag = "F")

######################### PONGO

# load Pongo Taj D values
taj_pon <- read.table(file = "tajimaD/pon_tajimaD.txt", header = TRUE)
attach(taj_pon)
# extract ponAbe2 length
length_ponAbe2 <- length[,c(1,4)]
length_ponAbe2 <- na.omit(length_ponAbe2)
length_ponAbe2$ponAbe2 <- length_ponAbe2$ponAbe2/1000

# extract Pongo abelii Taj D
D <- taj_pon[species == "P.abelii",1:3]
D <- na.omit(D)
# merge length and Taj D
D <- inner_join(D, length_ponAbe2, by = "gene")
# ggplot
gg_abelii <- ggplot(data = D) +
  geom_jitter(aes(x = ponAbe2, y = D_value), alpha = 0.7, colour = "#B8DE29FF") +
  geom_hline(yintercept = 0) +
  labs(x = "longueur (kb)", y = "D de Tajima", title = "P.abelii", tag = "G")

# extract Pongo pygmaeus Taj D
D <- taj_pon[species == "P.pygmaeus",1:3]
D <- na.omit(D)
# merge length and Taj D
D <- inner_join(D, length_ponAbe2, by = "gene")
# ggplot
gg_pygmaeus <- ggplot(data = D) +
  geom_jitter(aes(x = ponAbe2, y = D_value), alpha = 0.7, colour = "#FDE725FF") +
  geom_hline(yintercept = 0) +
  labs(x = "longueur (kb)", y = "D de Tajima", title = "P.pygmaeus", tag = "H")

multi <- grid.arrange(gg_pani, gg_troglo, gg_schwein, gg_ellioti, gg_verus, gg_gor, gg_abelii, gg_pygmaeus, nrow = 3)

ggsave(filename = "TajD_length.png", plot = multi, path = "tajimaD/", units = "cm", height = 30, width = 30)
